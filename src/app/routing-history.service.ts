import { Injectable } from '@angular/core';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoutingHistoryService {
  history = [];

  constructor(private router: Router) {
    this.router.events.pipe(filter(e => e instanceof NavigationEnd)).
      subscribe((e: RouterEvent) => {
        if (this.history.length > 0) {
          this.history = [this.history[this.history.length - 1], e.url];
        } else {
          this.history = [e.url];
        }
      });
  }

  getPreviousUrl() {
    return this.history[0];
  }
}
