import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Component1Component } from './component1/component1.component';
import { Component2Component } from './component2/component2.component';
import { RouteReuseStrategy } from '@angular/router';
import { CustomRouteReuseStrategy } from './navigation-strategy';
import { RoutingHistoryService } from './routing-history.service';

@NgModule({
  declarations: [
    AppComponent,
    Component1Component,
    Component2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: CustomRouteReuseStrategy },
    RoutingHistoryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private service: RoutingHistoryService) { }
}
