import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoutingHistoryService } from '../routing-history.service';

@Component({
  selector: 'app-component2',
  templateUrl: './component2.component.html',
  styleUrls: ['./component2.component.css']
})
export class Component2Component implements OnInit {

  constructor(private router: Router, private history: RoutingHistoryService) { }

  ngOnInit() {
  }
  navigateBack() {
    const lastURl = this.history.getPreviousUrl();
    this.router.navigate([lastURl]);
  }
}
